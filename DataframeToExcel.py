# 2019 by MaxPao
# 將Df儲存成Excel
import xlwt

def saveToExcel(filepath='download_csv/default.xls',sheetsName=['sheet1'],dfList=[]):
    if filepath=='':
        return
    if len(sheetsName) != len(dfList):
        print('sheetsName and dfList must be same length')
        return
    elif len(sheetsName)==0:
        print('No has any sheets name')
        return
    # borders = xlwt.Borders() # Create Borders
    # borders.left = xlwt.Borders.HAIR # May be: NO_LINE, THIN, MEDIUM, DASHED, DOTTED, THICK, DOUBLE, HAIR, MEDIUM_DASHED, THIN_DASH_DOTTED, MEDIUM_DASH_DOTTED, THIN_DASH_DOT_DOTTED, MEDIUM_DASH_DOT_DOTTED, SLANTED_MEDIUM_DASH_DOTTED, or 0x00 through 0x0D.
    # borders.right = xlwt.Borders.HAIR
    # borders.top = xlwt.Borders.HAIR
    # borders.bottom = xlwt.Borders.HAIR
    borders.left_colour = 0x40
    borders.right_colour = 0x40
    borders.top_colour = 0x40
    borders.bottom_colour = 0x40
    book = xlwt.Workbook(encoding="utf-8")
#     book.set_colour_RGB(0x21, 251, 228, 228)
    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN # May be: NO_PATTERN, SOLID_PATTERN, or 0x00 through 0x12
    pattern.pattern_fore_colour = 0x16 # May be: 8 through 63. 0 = Black, 1 = White, 2 = Red, 3 = Green, 4 = Blue, 5 = Yellow, 6 = Magenta, 7 = Cyan, 16 = Maroon, 17 = Dark Green, 18 = Dark Blue, 19 = Dark Yellow , almost brown), 20 = Dark Magenta, 21 = Teal, 22 = Light Gray, 23 = Dark Gray, the list goes on...
#   pattern.pattern_back_colour= 0xffffffee
    style1 = xlwt.XFStyle()
    style1.pattern = pattern
    style1.borders = borders # Add Borders to Style    
    
    pattern = xlwt.Pattern()
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN # May be: NO_PATTERN, SOLID_PATTERN, or 0x00 through 0x12
    pattern.pattern_fore_colour = 0x2B # May be: 8 through 63. 0 = Black, 1 = White, 2 = Red, 3 = Green, 4 = Blue, 5 = Yellow, 6 = Magenta, 7 = Cyan, 16 = Maroon, 17 = Dark Green, 18 = Dark Blue, 19 = Dark Yellow , almost brown), 20 = Dark Magenta, 21 = Teal, 22 = Light Gray, 23 = Dark Gray, the list goes on...
#     pattern.pattern_back_colour= 0xffffffdd
    style2 = xlwt.XFStyle()
    style2.pattern = pattern  
    style2.borders = borders # Add Borders to Style    
    
    for i in range(0,len(sheetsName)):
        print('sheet name:',i)
        sheet1 = book.add_sheet(sheetsName[i])
        dates = []
        colors = []
        _row=1
        _column=0
        df=dfList[i]
        for col in df.columns:
            sheet1.write(0, _column, col)
            _column+=1
        _row=1
        _column=0
        
        for i in range(0,len(df)):
            p=df.iloc[i]
            _column=0
            for value in p:
                v=value
                if value is None:
                    v=''
                elif type(value)==np.int64:
                    print('numpy.int64')
                if i%2==0:
                    sheet1.write(_row, _column, v,style1)                
                else:
                    sheet1.write(_row, _column, v,style2)                
                _column+=1
            _row+=1
    book.save(filepath)
    print('Done')




saveToExcel('data.xls',sheetsName=['sheet1','sheet2'],dfList=[df1,df2])